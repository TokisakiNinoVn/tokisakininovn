
<div  align="center">
    <h2 align="center"> Haloo!👋 I'm Nino! Nice to meet you!</h2>
    <h2 align="center">💖Queen-Code-Anime-Game-Music💖</h2>
    <img src="./images/nsme.gif" alt="Elysia">
</div>

<h1 align="center">
    <a href="https://nino.is-a.dev/">
        My website
    </a>
</h1>


# 🍀Tokisaki Nino's Personal Readme
- **FullName:** Tokisaki Nino.
- **Nickname:** Nino.
- **Age:** 18.3.
- **Gender:** Unknown.


## 🍀Introduce

Welcome to my personal page! I'm Nino, a solo/duo player. <br>
Game - Anime - Music - Queen.

## 🍀Skill

- **[Programming Language]:** Js, Py, Java,...
- **[Development Tools]:** VSC, VS,...
- **[Soft Skills]:** B.

## 🍀Education

- **[University/Institute]:** ICTU.
- **[High School]:** Raizen High School.

## 🍀Contact me

- **GitHub:** [TokisakiNinoVn](https://github.com/TokisakiNinoVn).
- **Discord:** [tokisakinino](link).
- **Twitter:** [_tokisaki_nino](https://twitter.com/_tokisaki_nino).
- **Instagram:** [nino.real.memories](https://www.instagram.com/nino.real.memories/).


## 🍀Proverb
What kind of person do you expect me to be? Normal partner? Self-training subjects? A social acquaintance... or... an important teammate? Based on your attitude and expectations, I will "play" or "become" one of them".

## Thank you for visiting my personal page!🥰